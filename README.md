KDEns
========

Code source de la [page web](https://dev.cof.ens.fr/klub-dev/) du Kub Dev


Modifier / Ajouter un article
-----------------------------

### Prérequis :

- [Hugo](https://gohugo.io/getting-started/installing/) : générateur de sites statiques


### Générer le site

- pour développer : lancer le serveur hugo avec `hugo -D server` et visiter
  l'adresse http://localhost:1313/klub-dev/ dans le navigateur
- pour déployer : simplement taper `hugo` et copier le dossier `public/` sur le serveur.
  **Pas besoin** d'installer hugo sur le serveur

### Ajouter un article

- `hugo new posts/toto.md`
- Éditer le fichier `content/posts/toto.md`, mettre l'option `draft` à `false` dans le header
