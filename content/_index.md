---
title: ""
date: 2019-06-03T21:22:14+02:00
draft: false
---

# Klub Dev ENS

Bienvenue sur le site du Klub Dev ENS ! Nous sommes un groupe de personnes
issues de la communauté normalienne qui prend soin des outils informatiques
utilisés dans la vie étudiante à l’ENS.

Plus d'informations sur les projets que nous menons dans la section [à propos]({{< relref "about.md" >}}).

## Actualités
