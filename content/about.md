---
title: "À propos"
date: 2019-02-25T21:46:29+01:00
draft: false
---

Le Klub Dev ENS (abrégé KDEns), c’est un groupe de personnes issues de la
communauté normalienne qui prend soin des outils informatiques utilisés
dans la vie étudiante à l’ENS. On développe et maintient des outils tels
que [GestioCOF](https://www.cof.ens.fr/gestion/), que tu connais
probablement, avec le système de tirage au sort du BdA ou encore
l’application K-Fêt, le [site de
l’Ernestophone](https://ernestophone.ens.fr), etc.

On fait aussi un peu d'administration système puisqu’on gère les
machines sur lesquelles tournent ces sites mais aussi d’autres choses
comme le serveur photo du COF par exemple.

## Pourquoi ?

On existe parce qu’un site ça vit, ça plante, fait des caprices, ça a
besoin d’un coup de jeune de temps en temps… Des gens trouvent des bugs
ou ont besoin de nouvelles fonctionnalités, d’une meilleure interface,
etc. On fait donc en sorte que tout ça tienne la route et on l’améliore
au fil du temps.

On a aussi déjà commencé des projets depuis zéro. Si quelqu’un (toi ?)
vient nous voir avec une idée sympa et qu’on a le temps de s’en occuper,
on peut envisager de se lancer dedans.

## Ça a l’air cool, comment je peux aider ?

Très bonne question !

Il y a plusieurs pistes pour ça. Pour commencer, on est toujours très
content·e·s de voir venir des nouvelles têtes donc n’hésite pas à nous
envoyer un mail à `klub-dev-ens (chez) ens (point) fr` ou à rejoindre
l'équipe [Klub Dev](https://merle.eleves.ens.fr/cof-geek) sur le serveur
de messagerie instantanée (Mattermost) de l’école pour faire notre
connaissance et nous montrer que tu existes (oui oui c’est très utile).
On te montrera où commencer en fonction de ce que tu veux faire et on
t’aidera du mieux qu’on peut à te familiariser avec la codebase.

Par ailleurs, tout notre code est accessible sur le [serveur git de
l’école](https://git.eleves.ens.fr/klub-dev-ens) (connecte-toi avec ton
compte clipper pour avoir accès à tout le contenu). N’hésite donc pas
non plus à aller regarder d’un peu plus près ce qu’on fait et en
particulier les [issues](https://git.eleves.ens.fr/klub-dev-ens/issues)
ouvertes. C’est là que sont listées les choses à faire, bugs à réparer,
etc. Regarde notamment les issues marquées `facile` qui sont plus
faciles, pas très urgentes (donc pas de pression) et parfois
accompagnées d’indications pour la résolution. Et n’oublie pas qu’on est
là pour t’aider : si tu ne comprends pas bien une issue ou si tu ne sais
pas où chercher le bout de code à modifier, n’hésite pas à demander.

## Oui d’accord mais moi je n’y connais rien…

Ce n’est pas un problème, nous non plus ! … au début ;)

Les outils qu’on utilise sont faciles d’accès et on sera ravi de guider
les débutant·e·s qui veulent apprendre à utiliser Python, Django, git,
etc. D'ailleurs la plupart d'entre nous a découvert Django avec Le Klub
Dev Ens et nous pensons que le club et un bon endroit pour apprendre.

De plus, on ne demande qu’à se diversifier et à rencontrer des gens avec
des profils différents des nôtres : si tu t’y connais en graphisme et
aimerais apprendre les bases de CSS / javascript, ou si tu penses que
GestioCOF devrait être traduit en anglais (et tu as raison) mais n’as
aucune idée de comment faire ça, tu es plus que bienvenu·e. On a très
certainement besoin de tes compétences et on sera ravis de partager les
nôtres.
