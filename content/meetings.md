---
title: "Réunions hebdomadaires"
date: 2019-10-05T16:19:34+01:00
draft: false
---

Les réunions hebdomadaires vont bientôt reprendre ! La salle et l'horaire
seront affichés sur cette page. N'hésitez pas à passez nous voir pour découvrir
le club ou nous parler de vos projets.

\* _Vous pouvez nous laisser un petit message avant de venir sur le
[mattermost](https://merle.eleves.ens.fr/cof-geek) des élèves pour vous assurez
que nous serons présent la semaine où vous comptez passer._
