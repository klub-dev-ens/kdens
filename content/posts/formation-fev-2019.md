---
title: "Formation Django le 16 février"
date: 2019-02-25T21:46:48+01:00
draft: false
---

## Version française

Le Klub Dev Ens organise ce samedi 16 février une initiation au développement web à l'ENS à l'aide
du framework Django (en python).

Cette formation s'adresse aussi bien à un public débutant qui veut découvrir la programmation qu'à
des des programmeur⋅se⋅s confirmé⋅e⋅s qui voudraient se mettre au web et/ou à Django. Nous
commencerons par parler du fonctionnement d'internet avant d'aller progressivement vers la
construction d'un site web. Ainsi vous

Infos pratiques :

- Date / lieu : samedi **16 février** de **11h à 15h** en **salle R** (passage rouge, sous-sols du
  DMA)
- **Soyez là dès le début**, la formation n'est pas pensée pour être prise en cours de route,
- En revanche, il est possible de venir seulement la première partie de la journée (par exemple si
  vous participez aux journées portes ouverts ou aux interludes)
- Nous serons là dès 10h30 avec du café (et des croissants ?) pour un accueil convivial
- **Venez avec votre ordinateur**, c'est indispensable pour apprendre à programmer
- Nous commanderons à manger ensemble vers 12h30

Venez nombreux⋅ses !

---

## English version

On the 16th of February (this Saturday!), the Klub Dev Ens organizes at the ENS an initiation to
web development using the Django framework (based on python).

There is no requirement to attend this formation. We expect to see beginners, willing to learn
programming, as well as confirmed programmers that would like to discover web programming / Django.
We will start by giving insights on how the internet works and then go gradually through the
process of developing a website.

Practical information:
- Date / place: Saturday **16th February** from **11h to 15h** in **room R** (red corridor, below
  the DMA)
- **Be here from 11am**, you won't be able to catch up if you arrive during the initiation
- It is however possible to leave earlier (for instance if you attend the interludes or the ENS
  open house day)
- We will be present from 10:30 with coffee (and croissants ?)
- **Bring your laptop**, you need it in order to learn programming
- We will order lunch together around 12h30

See you there!
