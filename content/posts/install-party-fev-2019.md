---
title: "Install party linux le samedi 9 février"
date: 2019-02-19T20:10:10+01:00
draft: false
---

## Version française

Envie d'essayer Linux ? Besoin d'un coup de main pour l'installer sur votre ordinateur ?

Le Klub Dev Ens organise le samedi 9 février en salle Weil une rencontre lors de laquelle des
utilisateurices plus expérimenté⋅e⋅s de Linux viendront aider les plus novices à l'installer et le
configurer sur leur ordinateur. Ce sera aussi l'occasion d'échanger autour de Linux, poser des
questions, partager des astuces ou des conseils, etc. Venez nombreux⋅ses !

Infos pratiques :

- date & lieu : le samedi 9 février en salle Weil de 10h à 17h
- l'installation ne devrait pas prendre plus d'une heure
- prérequis : bien que l'installation échoue rarement, nous ne sommes jamais à l'abri d'une
  erreur : il est donc *très fortement conseillé* de sauvegarder ses données sur une clef usb ou un
  disque dur externe avant de venir


---
## English version

Ever wanted to try Linux? Need some help to install it your laptop?

The Klub Dev Ens organizes an install fest on Saturday, February 9th in room Weil. An install fest
is an event during which experienced users help newcomers to install and configure Linux on their
computer. It is also an opportunity to share ones experience with other users, ask questions, share
some tips and tricks, etc. See you there!

Practical information:
- date & place: Saturday, February 9th in room Weil from 10am to 5pm.
- installation should not last more than a hour
- prerequisite: installations usually do not fail, however it is highly recommended to make a back
  up of your data on an external drive before coming
