#!/bin/sh

set -euC

echo "Build the website…"
hugo

echo "Backup old kde@www.eleves:www…"
ssh kde 'tar cf www.tgz www && rm -r www'

echo "Copy to kde@www.eleves:www…"
scp -r public kde:www
